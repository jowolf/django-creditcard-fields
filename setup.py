#!/usr/bin/env python

from distutils.core import setup

setup(name='django-creditcard-fields',
      version='2.0',  # Django 2 / Python 3 compatible
      description='Credit Card Fields for Django, Django 2 / Python 3 compatible',
      author='Joseph Wolff, from Jordan Reiter, Bryan Chow',
      author_email='joe@eracks.com',
      url='https://gitlab.com/jowolf/django-creditcard-fields',
      packages=['django_creditcard'],
     )
